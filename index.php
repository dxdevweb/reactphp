<?php

use React\HttpClient\Client;
use React\HttpClient\Response;

require __DIR__ . '/vendor/autoload.php';

$loop = React\EventLoop\Factory::create();
$client = new Client($loop);

$request = $client->request('GET', 'https://facebook.com');

$request->on('response', function (Response $response) {
    var_dump($response->getContent());

    $response->on('div', function ($chunk) {
        echo $chunk->text();
    });
    
    $response->on('end', function () {
        echo 'DIOGO' . PHP_EOL;
    });
});

$request->end();

$loop->run();
